# gitlab-runner-config

> Notice: If you came her for the more secure dind variant checkout the branch [more-secure-dind](https://gitlab.com/nidomiro.de/server/gitlab-runner-config/-/tree/more-secure-dind)

This Repo contains the basic setup for my GitLab Runners.
My Runners now uses DooD (Docker out of Docker) combined with a rootless docker install.

## Setup

This setup uses [ansible](https://www.ansible.com/)

Example setup
```shell
ansible-playbook -i ../root-server-config/inventory.yml playbook.yml -e CI_SERVER_URL=https://gitlab.com/ -e CI_SERVER_TOKEN=**************
```

> Notice: The playbook is not perfect right now. You might have to execute the Playbook - it will fail. Then execute `sudo machinectl shell {{ gitlab_runner_user }}@ /usr/bin/dockerd-rootless-setuptool.sh install` on the remote host and run the playbook again. It should now finish successfully.
